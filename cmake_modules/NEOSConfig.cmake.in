set(NEOS_VERSION @VERSION_MAJOR@.@VERSION_MINOR@)

# relocatable package
@PACKAGE_INIT@

set_and_check(NEOS_INC_DIR "@PACKAGE_INC_INSTALL_DIR@")
set_and_check(NEOS_LIB_DIR "@PACKAGE_LIB_INSTALL_DIR@")

check_required_components(NEOS)

# need MORSE Find modules: necessary files must be distributed in the install path
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_LIST_DIR}/find")

# dependencies of NEOS
include(CMakeFindDependencyMacro)

if (@BUILD_CORE@ OR @BUILD_GEOMETRY@)
  find_package(BITPIT REQUIRED)
endif()

if (@BUILD_LAPLACIAN@)
  find_package(PETSC REQUIRED)
endif()

if (@ENABLE_MPI@)
  find_package(MPI REQUIRED)
endif()

if (@USE_LAPACKE@)
  find_package(LAPACKE REQUIRED)
endif()

if (@USE_EIGEN3@)
  find_package(Eigen3 REQUIRED)
endif()

include("${CMAKE_CURRENT_LIST_DIR}/neosTargets.cmake")
