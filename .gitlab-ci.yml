cache:
  paths:
    - /cache

stages:
  - docker-base
  - docker-neos
  - docker-notebook
  - pages
  - build-lib
  - test
  - sonar


.build-lib:
  tags:
    - ci.inria.fr
  script:
    - git submodule init && git submodule update
    - mkdir -p build && cd build && cmake .. -DBUILD_SHARED_LIBS=ON -DBUILD_CORE=ON -DBUILD_LAPLACIAN=ON -DBUILD_GEOMETRY=ON -DENABLE_MPI=ON -DBUILD_MATHS=ON -DCMAKE_VERBOSE_MAKEFILE=ON -DBUILD_EXAMPLES=ON
    - make -j8
  artifacts:
    paths:
      - build

build-lib-master:
  stage: build-lib
  only:
    - master
  image: registry.gitlab.inria.fr/memphis/neos/base:latest
  extends: .build-lib

build-lib:
  stage: build-lib
  except:
    - master
  image: registry.gitlab.inria.fr/memphis/neos/base:latest
  #image: registry.gitlab.inria.fr/memphis/neos/base/$CI_COMMIT_BRANCH:latest
  extends: .build-lib

.test:
  tags:
    - ci.inria.fr
  artifacts:
    name: neos_test
    expire_in: 1 day
    paths:
      - neos-build.log
      - neos.lcov
      - neos-coverage.xml
      - neos-cppcheck.xml
      - neos-rats.xml
      - neos-vera.xml
      - build/analyzer_reports/
  script:
    - mkdir -p build && cd build && cmake .. -DBUILD_SHARED_LIBS=ON -DBUILD_CORE=ON -DBUILD_LAPLACIAN=ON -DBUILD_GEOMETRY=ON -DENABLE_MPI=ON -DBUILD_MATHS=ON -DCMAKE_VERBOSE_MAKEFILE=ON -DBUILD_EXAMPLES=ON -DBUILD_TESTS=ON -DCMAKE_VERBOSE_MAKEFILE=ON  -DCMAKE_CXX_FLAGS="-O0 -g -fPIC --coverage -Wall -fdiagnostics-show-option -fno-inline" -DCMAKE_EXE_LINKER_FLAGS="--coverage"
    - scan-build -plist --intercept-first --analyze-headers -o analyzer_reports make | tee ../neos-build.log
    - ctest -V
    - ./examples/transport 0.1 2 2 3 0.1
    - cd ..
    - lcov --directory . --capture --output-file neos.lcov
    - python -m lcov_cobertura neos.lcov --output neos-coverage.xml
    - gcovr -r .
    - export SOURCES_TO_ANALYZE="src tests examples"
    - cppcheck -v -f --language=c++ --platform=unix64 --enable=all --xml --xml-version=2 --suppress=missingIncludeSystem ${SOURCES_TO_ANALYZE} 2> neos-cppcheck.xml
    - rats -w 3 --xml ${SOURCES_TO_ANALYZE} > neos-rats.xml
    - bash -c 'find ${SOURCES_TO_ANALYZE} -regex ".*\.cpp\|.*\.hpp" | vera++ - -showrules -nodup |& $HOME/sonar/sonar-cxx/cxx-sensors/src/tools/vera++Report2checkstyleReport.perl > neos-vera.xml'

test-master:
  stage: test
  only:
    - master
  image: registry.gitlab.inria.fr/memphis/neos/base:latest
  extends: .test

test:
  stage: test
  except:
    - master
  #image: registry.gitlab.inria.fr/memphis/neos/base/$CI_COMMIT_BRANCH:latest
  image: registry.gitlab.inria.fr/memphis/neos/base:latest
  extends: .test

sonar:
  image: registry.gitlab.inria.fr/memphis/neos/base:latest
  tags:
    - ci.inria.fr
  stage: sonar
  dependencies:
  - test
  artifacts:
    name: neos_sonar
    expire_in: 1 day
    paths:
      - sonar.log
  script:
    - sonar-scanner -X -Dsonar.login=$SONAR_TOKEN > sonar.log
  only:
   - master

docker-base-master:
  stage: docker-base
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/base
  only:
   - master
  extends: .docker-base

docker-base:
  stage: docker-base
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/base
    #IMAGE: $CI_REGISTRY_IMAGE/base/$CI_COMMIT_BRANCH
  except:
   - master
  extends: .docker-base

.docker-base:
  image: "buildah/buildah"
  variables:
    STORAGE_DRIVER: "vfs"
    BUILDAH_FORMAT: "docker"
  before_script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  when: manual
  tags:
    - dind
  script:
    - docker pull               $IMAGE:latest || true
    - docker build              --tag $IMAGE:$CI_COMMIT_SHA --tag $IMAGE:latest . -f docker/Dockerfile-base
    - docker push               $IMAGE:$CI_COMMIT_SHA
    - docker push               $IMAGE:latest

docker-neos-master:
  stage: docker-neos
  when: manual
  variables:
    IMAGE: $CI_REGISTRY_IMAGE
    IMAGE_BASE: $CI_REGISTRY_IMAGE/base
    IMAGE_TAG: latest
  only:
    - master
  extends: .docker-neos

docker-neos-branch:
  stage: docker-neos
  when: manual
  variables:
    IMAGE: $CI_REGISTRY_IMAGE
    IMAGE_BASE: $CI_REGISTRY_IMAGE/base/$CI_COMMIT_BRANCH
    IMAGE_TAG: $CI_COMMIT_BRANCH
  only:
    - branches
  extends: .docker-neos

docker-neos-tags:
  stage: docker-neos
  when: manual
  variables:
    IMAGE: $CI_REGISTRY_IMAGE
    IMAGE_BASE: $CI_REGISTRY_IMAGE/base
    IMAGE_TAG: $CI_COMMIT_TAG
  only:
    - tags
  extends: .docker-neos

.docker-neos:
  image: "buildah/buildah"
  variables:
    STORAGE_DRIVER: "vfs"
    BUILDAH_FORMAT: "docker"
  before_script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  tags:
    - dind
  script:
    - docker pull               $IMAGE:$IMAGE_TAG || true
    - docker build              --tag $IMAGE:$IMAGE_TAG --build-arg IMAGE_BASE=$IMAGE_BASE . -f docker/Dockerfile
    - docker push               $IMAGE:$IMAGE_TAG

docker-notebook:
  stage: docker-notebook
  when: manual
  variables:
    IMAGE: $CI_REGISTRY_IMAGE/notebook
  only:
    - master
  extends: .docker-notebook

.docker-notebook:
  image: "buildah/buildah"
  variables:
    STORAGE_DRIVER: "vfs"
    BUILDAH_FORMAT: "docker"
  before_script:
  - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  tags:
    - dind
  script:
    - cd docker/notebook
    - docker pull               $IMAGE:latest || true
    - docker build --cache-from $IMAGE:latest --tag $IMAGE:$CI_COMMIT_SHA --tag $IMAGE:latest .
    - docker push               $IMAGE:$CI_COMMIT_SHA
    - docker push               $IMAGE:latest

pages:
  image: registry.gitlab.inria.fr/memphis/neos/base:latest
  when: manual
  tags:
  - ci.inria.fr
  script:
  - mkdir -p build && cd build && cmake ..  -DBUILD_DOCUMENTATION=ON  -DBUILD_CORE=ON
  - cd doc && make doc && mv html ../../public
  artifacts:
    paths:
    - public
  only:
  - doc
