# Neos Authors

## Steering Comittee

- Michel Bergmann / INRIA
- Angelo Iollo / UB (Bordeaux University)

## Scientific Contributors (Alphabetical Order)

- Michel Bergmann / INRIA
- Florian Bernard / INRIA
- Antoine Fondanèche / INRIA
- Angelo Iollo / UB (Bordeaux University)

## Software Contributors (Alphabetical Order)

- Philippe Depouilly / CNRS
- Laurent Facq / CNRS
- Antoine Gérard / INRIA
- Matias Hastaran / INRIA
- Florent Pruvost / INRIA
