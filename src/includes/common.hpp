/* -------------------------------------------------------------------------*\
 *
 *  NEOS
 *
 *  -------------------------------------------------------------------------
 *  License
 *  This file is part of Neos.
 *
 *  Neos is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU Lesser General Public License v3 (LGPL)
 *  as published by the Free Software Foundation.
 *
 *  Neos is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
 *  License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with Neos. If not, see <http://www.gnu.org/licenses/>.
 *
\*---------------------------------------------------------------------------*/

/**
 * @file common.hpp
 *
 * Neos is a software package provided by Inria Bordeaux - Sud-Ouest
 *
 * @version 0.0
 * @author Hastaran Matias
 * @date 2018-02-02
 *
 */

#include <array>
#include <vector>

#ifndef COMMON_HPP
#define COMMON_HPP

#define GRID_3D 3
#define GRID_2D 2
#define NPoint std::array<double, 3>
#define NPX 0
#define NPY 1
#define NPZ 2

//#include "NeosPiercedVector.hpp"

namespace neos {

typedef std::array<double, 3>		darray3;
typedef std::vector<darray3>		darray3vector;

/*! enum class for laplacian algo type */
enum lapType {
  FINITEVOLUME,   /*!< Finite Volume */
  FINITEDIFF      /*!< Finite difference */
};

/*! enum class for solver selection (for laplacian) */
enum solverType {
  PETSC,   /*!< Use PETSC */
  OTHER    /*!< Use .... */
};

/*! enum class for laplacian BCPosition */
// enum BCPosition {
//     OnInterface=0.5, /*!< On interface */
//     OnOuterCell=1.0  /*!< Outer cell */
// };
struct BCPosition
{
  static constexpr double OnInterface = 0.5;
  static constexpr double OnOuterCell = 1.0;
};

/*! enum class for Interpolation algo type */
enum interpoType {
  DISTANCEWEIGHTED,
  POLY2D,
  POLY3D,
  RBF,
  MLS
};

enum Var {
  Ux,   // x-component of velocity
  Uy,   // y-component of velocity
  Uz,   // z-component of velocity
  P,   //Pressure
  LS,   //Level set
};

enum RHSOpType {
  ADD,
  SET
};

}

#endif /*  COMMON_HPP */
