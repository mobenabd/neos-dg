# -------------------------------------------------------------------------
#
#  NEOS
#
#  -------------------------------------------------------------------------
#  License
#  This file is part of Neos.
#
#  Neos is free software: you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License v3 (LGPL)
#  as published by the Free Software Foundation.
#
#  Neos is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
#  License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with Neos. If not, see <http://www.gnu.org/licenses/>.
#
#---------------------------------------------------------------------------

set(NEOS_LIB_SRCS "")
set(NEOS_DEP_LIBS "")

set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

if (BUILD_CORE AND BITPIT_FOUND)
  list(APPEND NEOS_LIB_SRCS
    core/NeosLevelSet.cpp
    core/ILevelSet.cpp
    core/BoundaryConditions.cpp
    core/Grid.cpp
    core/Transport.cpp
    core/NeosInterface.cpp
    core/CommonTools.cpp
    core/InputFileReader.cpp
    core/SimulationParameters.cpp
    core/StencilBuilder.cpp
    )
endif()

if (BUILD_GEOMETRY AND BITPIT_FOUND)
  list(APPEND NEOS_LIB_SRCS
    geometry/Geo3D.cpp
    geometry/Geo2D.cpp
    geometry/Circle.cpp
    geometry/Sphere.cpp
    geometry/Cylinder.cpp
    geometry/ACylinder.cpp
    geometry/ASphere.cpp
    geometry/STLGeometry.cpp
    )
endif()

if (BUILD_MATHS)
  if (BUILD_INTERPOLATOR)
    if (USE_LAPACKE)
      find_package(LAPACKE REQUIRED)
      # may be required when using mkl as lapack, we still need lapacke.h which is not providen by mkl
      find_path(LAPACKE_INCLUDE_DIRS NAMES lapacke.h HINTS ${LAPACKE_INCLUDE_DIRS} ${LAPACKE_ROOT}/include $ENV{LAPACKE_ROOT}/include)
    endif()
    if (USE_EIGEN3)
      find_package(Eigen3 REQUIRED)
    endif()
    list(APPEND NEOS_LIB_SRCS maths/interpolator/Polynomial2D.cpp)
    list(APPEND NEOS_LIB_SRCS maths/interpolator/Polynomial3D.cpp)
    list(APPEND NEOS_LIB_SRCS maths/interpolator/RBF.cpp)
    list(APPEND NEOS_LIB_SRCS maths/interpolator/Distance.cpp)
    list(APPEND NEOS_LIB_SRCS maths/interpolator/MLS2D.cpp)
    list(APPEND NEOS_LIB_SRCS maths/interpolator/InterpolatorFactory.cpp)
  endif()
  if (BUILD_CORE AND BITPIT_FOUND AND BUILD_LAPLACIAN)
    list(APPEND NEOS_LIB_SRCS  maths/Laplacian.cpp)
    list(APPEND NEOS_LIB_SRCS  maths/solvers/LaplacianPetsc.cpp)
#    list(APPEND NEOS_LIB_SRCS  maths/AliceLaplacian.cpp)
    list(APPEND NEOS_LIB_SRCS  maths/LaplacianFactory.cpp)
    list(APPEND NEOS_DEP_LIBS  ${PETSC_LIBRARIES})
  endif()
  list(APPEND NEOS_LIB_SRCS
    maths/Function.cpp
    )
  if (BITPIT_FOUND)
    list(APPEND NEOS_LIB_SRCS maths/Gradient.cpp)
  endif()
endif()

add_library(neos ${NEOS_LIB_SRCS})

target_include_directories(neos PUBLIC
  $<BUILD_INTERFACE:${NEOS_SOURCE_DIR}/src/core>
  $<BUILD_INTERFACE:${NEOS_SOURCE_DIR}/src/geometry>
  $<BUILD_INTERFACE:${NEOS_SOURCE_DIR}/src/includes>
  $<BUILD_INTERFACE:${NEOS_SOURCE_DIR}/src/maths>
  $<BUILD_INTERFACE:${NEOS_SOURCE_DIR}/src/maths/interpolator>
  $<INSTALL_INTERFACE:include>)

if(BUILD_CORE OR BUILD_GEOMETRY)
  target_link_libraries(neos PUBLIC ${BITPIT_LIBRARIES})
  target_include_directories(neos PUBLIC ${BITPIT_INCLUDE_DIR})
endif()

if (BUILD_LAPLACIAN)
  target_link_libraries(neos PUBLIC ${PETSC_LIBRARIES})
  target_compile_definitions(neos PUBLIC USE_LAPLACIAN)
  target_include_directories(neos PUBLIC ${PETSC_INCLUDE_DIRS})
endif()

if (ENABLE_MPI)
  target_link_libraries(neos PUBLIC MPI::MPI_CXX)
  target_compile_definitions(neos PUBLIC ENABLE_MPI)
  target_compile_definitions(neos PUBLIC BITPIT_ENABLE_MPI)
endif()

if (USE_LAPACKE)
  target_link_libraries(neos PUBLIC ${LAPACKE_LIBRARIES} ${LAPACK_LIBRARIES})
  target_compile_definitions(neos PUBLIC USE_LAPACKE)
  target_include_directories(neos PUBLIC ${LAPACKE_INCLUDE_DIRS})
endif()

if (USE_EIGEN3)
  target_compile_definitions(neos PUBLIC USE_EIGEN3)
  target_include_directories(neos PUBLIC ${EIGEN3_INCLUDE_DIR})
endif()

if (BUILD_CORE)
  string(APPEND INCLUDE_FILES "#include <NeosLevelSet.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <ILevelSet.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <Grid.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <Utils.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <UserDataComm.hpp>\n")
endif()

if (BUILD_GEOMETRY)
  string(APPEND INCLUDE_FILES "#include <ACylinder.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <Circle.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <Cylinder.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <ASphere.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <Sphere.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <STLGeometry.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <LaplacianFactory.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <Gradient.hpp>\n")
  string(APPEND INCLUDE_FILES "#include <Transport.hpp>\n")
endif()

if (ENABLE_MPI)
  string(APPEND INCLUDE_FILES "#include <mpi.h>\n")
endif()

configure_file("${CMAKE_CURRENT_SOURCE_DIR}/core/Neos.hpp.in"
  "${CMAKE_CURRENT_SOURCE_DIR}/core/Neos.hpp" @ONLY)

# CPPCheck custom command
if (USE_CPPCHECK)
  find_package(CPPCHECK QUIET)
  add_custom_command(TARGET neos POST_BUILD
    COMMAND ${CPPCHECK_EXECUTABLE}  --enable=all -f --check-config --language=c++ --platform=unix64 --suppress=missingIncludeSystem ${CMAKE_CURRENT_SOURCE_DIR} -I./core -I./geometry -I./maths -I${BITPIT_INCLUDE_DIRS} -I${LAPACKE_INCLUDE_DIRS}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
endif()

###############################################################################
# Export and Install targets #
##############################

# export target neos
install(EXPORT neosTargets
        NAMESPACE neos::
        DESTINATION lib/cmake/neos
        )
# installation of target neos
install(TARGETS neos
        EXPORT neosTargets
        ARCHIVE DESTINATION lib
        LIBRARY DESTINATION lib
        )

file(GLOB_RECURSE files "${CMAKE_CURRENT_SOURCE_DIR}/*.hpp" "${CMAKE_CURRENT_SOURCE_DIR}/*.tpp")
install(FILES ${files} DESTINATION include)

# see https://cmake.org/cmake/help/latest/module/CMakePackageConfigHelpers.html
include(CMakePackageConfigHelpers)

set(INC_INSTALL_DIR "include/" CACHE STRING "where to install headers relative to prefix" )
set(LIB_INSTALL_DIR "lib/" CACHE STRING "where to install libraries relative to prefix" )

configure_package_config_file(../cmake_modules/NEOSConfig.cmake.in
                              ${CMAKE_CURRENT_BINARY_DIR}/NEOSConfig.cmake
                              INSTALL_DESTINATION ${LIB_INSTALL_DIR}/cmake/neos
                              PATH_VARS INC_INSTALL_DIR LIB_INSTALL_DIR)
write_basic_package_version_file(NEOSConfigVersion.cmake
                                 VERSION ${VERSION_MAJOR}.${VERSION_MINOR}
                                 COMPATIBILITY AnyNewerVersion)

# Install config files
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/NEOSConfig.cmake ${CMAKE_CURRENT_BINARY_DIR}/NEOSConfigVersion.cmake
        DESTINATION ${LIB_INSTALL_DIR}/cmake/neos)

# need MORSE Find modules: necessary files must be distributed in the install path
set(morse_dependencies "")
if (USE_LAPACKE)
  list(APPEND morse_dependencies "LAPACKE;LAPACKEXT")
endif()
if (USE_EIGEN3)
  list(APPEND morse_dependencies "Eigen3")
endif()
if (BUILD_LAPLACIAN)
  list(APPEND morse_dependencies "PETSC")
endif()
morse_install_finds(morse_dependencies ${LIB_INSTALL_DIR}/cmake/neos/find)